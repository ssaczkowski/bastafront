package app.controller;

import basta.model.BastaModel;
import basta.model.Prediction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Controller
public class GeneralController {


    @GetMapping("/index")
    public String index(Model model) {
        return "index.html";
    }

    List<Prediction> predictionList = new ArrayList<>();
    List<Prediction> cyclesList = new ArrayList<>();

    @PostConstruct
    public void init() throws Exception {
        predictionList = BastaModel.getInstance().getPredictions();
        //cyclesList = BastaModel.getInstance().getCycles();

    }

    @GetMapping("/predictions")
    public String getPredictions(Model model) {
        model.addAttribute("predictions", predictionList);
        return "predictions.html";
    }

    @GetMapping("/cycles")
    public String getCycles(Model model) {
        model.addAttribute("cycles", cyclesList);
        return "cycles.html";
    }

}
